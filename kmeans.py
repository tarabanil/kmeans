from __future__ import division
import numpy
import random
import os
import json as JSON
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem.porter import *
from math import ceil, sqrt
def get_dict(A):
    d = dict()
    lmtzr = WordNetLemmatizer()
    stemmer = PorterStemmer()
    for e in A:
        if e.has_key("summary"):
            temp = nltk.pos_tag(e['summary'].split())
            for i in temp:
                if ((i[1] == 'NN') or (i[1] == 'NNP')) and (i[0].find("last.fm") == -1) and (i[0].find("class=") == -1)\
                         and (i[0].find("rel=") == -1) and (len(i[0]) > 3) and (i[0].find("href") == -1)\
                        and (i[0].find("&amp") == -1) and (i[0].find("&quot") == -1):
                    #k = lmtzr.lemmatize(i[0].lower())
                    k = stemmer.stem(i[0].lower())
                    if d.has_key(k):
                        d[k] = d[k] + 1
                    else:
                        d.update({k : 0})
    for e in d.keys():
        if d[e] < ceil(sqrt(len(A)) / 2):
            d.pop(e)
    print "number of features:", len(d)
    return d


def get_fest(A):
    fest = dict()
    for e in A:
        if e.has_key("festivals"):
            for i in e["festivals"]:
                if fest.has_key(i):
                    fest[i] = fest[i] + 1
                else:
                    fest.update({i : 0})

    for e in fest.keys():
        if fest[e] < ceil(sqrt(len(A)) / 5):
            fest.pop(e)
    return fest


def get_features(A):
    lmtzr = WordNetLemmatizer()
    stemmer = PorterStemmer()
    d = get_dict(A).keys()
    fest = get_fest(A).keys()
    features_n = len(d) + len(fest)
    X = numpy.zeros(len(A) * features_n)
    X.shape = (len(A), features_n)
    for e in range(0, len(A)):
        if A[e].has_key("summary"):
            temp = nltk.pos_tag(A[e]['summary'].split())
            tempset = set()
            for i in temp:
                if ((i[1] == 'NN') or (i[1] == 'NNP')) and (i[0].find("last.fm") == -1) and (i[0].find("class=") == -1)\
                         and (i[0].find("rel=") == -1) and (len(i[0]) > 1) and (i[0].find("href") == -1):
                    k = stemmer.stem(i[0].lower())
                    tempset.add(k)
            for l in list(tempset):
                for j in range(0, len(d)):
                    if l.lower() == d[j]:
                        X[e][j] += 1

        if A[e].has_key("festivals"):
            for i in A[e]["festivals"]:
                for j in range(0, len(fest)):
                    if i == fest[j]:
                        X[e][j + len(d)] += 1

    return X

def get_tags_number(X):
    tags = set()
    for e in X:
        if e.has_key("main_tag"):
            tags.add(e["main_tag"])
    return len(tags)

def get_artists():
    jSonArray = []
    for filename in os.listdir('artists'):
        file = open('./artists/' + filename, 'r')
        json = JSON.load(file)
        jSonArray += json
    return jSonArray

def scores(y, X):
    TP = 0
    TN = 0
    FN = 0
    FP = 0
    for e in range(0, len(y)):
        for k in range(0, len(y)):
            if k != e:
                xe = X[e]
                xk = X[k]
                tags1 = xe["tags"]
                tags1.append(xe["main_tag"])
                tags2 = xk["tags"]
                tags2.append(xk["main_tag"])
                inter =  list(set(tags1) & set(tags2))
                if y[e] == y[k]:
                    if len(inter) != 0:
                        TP = TP + 1
                    else:
                        FP = FP + 1
                else:
                    if len(inter) != 0:
                        FN = FN + 1
                    else:
                        TN = TN + 1
    fscore = 2 * TP / (2 * TP + FP + FN)
    acc = (TN + TP) / (TP + TN + FN + FP)
    print fscore
    print acc
    return acc, fscore

class Kmeans(object):

    def __init__(self, n_clusters=8, init='k-means++', n_init=10, max_iter=300, tol=0.0001,
                 precompute_distances=True, verbose=0, random_state=None, copy_x=True, n_jobs=1):
        self.n_clusters = n_clusters
        self.max_iter = max_iter
        self.n_init = n_init
        self.precompute_distances = precompute_distances
        self.tol = tol
        self.n_jobs = n_jobs
        self.random_state = random_state
        self.init = init
        self.verbose = verbose
        self.copy_x = copy_x

    def fit(self, X, y=None):
        random.seed()
        s = X.shape
        centr = numpy.zeros(self.n_clusters * s[1])
        centr.shape = (self.n_clusters, s[1])
        r = numpy.zeros(s[0] * self.n_clusters)
        r.shape = (s[0], self.n_clusters)
        for i in range(0, self.n_clusters):
            centr[i] = X[random.randint(0, s[0] - 1)]
        for j in (0, self.max_iter):
            for k in range(0, s[0]):
                temp = ((X[k] - centr)**2).sum(1)
                arg = temp.argmin()
                r[k] = temp != temp
                r[k][arg] = 1
            sum = r.sum(0)
            sum.shape = (sum.size, 1)
            centr = numpy.dot(numpy.transpose(r), X) / sum
        bestL = ((numpy.dot(r, centr) - X)**2).sum()
        bestCentr = centr
        bestR = r


        for i in range(1, self.n_init):
            for i in range(0, self.n_clusters):
                centr[i] = X[random.randint(0, s[0] - 1)]

            for j in (0, self.max_iter):
                for k in range(0, s[0]):
                    temp = ((X[k] - centr)**2).sum(1)
                    arg = temp.argmin()
                    r[k] = temp != temp
                    r[k][arg] = 1

            sum = r.sum(0)
            sum.shape = (sum.size, 1)
            centr = numpy.dot(numpy.transpose(r), X) / sum
            L = ((numpy.dot(r, centr) - X)**2).sum()
            if L < bestL:
                bestCentr = centr
                bestR = r
                bestL = L

        self.cluster_centers_ = bestCentr
        self.labels_ = bestR
        self.inertia_ = bestL


    def fit_predict(self, X):
        self.fit(X)
        return self.labels_
    def fit_transform(self, X, y=None):
        self.fit(X)
        return self.transform(X)

    def get_params(self, deep = True):

        if deep:
            l = {"n_clusters" : self.n_clusters,"max_iter" : self.max_iter, "n_init" : self.n_init,
            "precompute_distances" : self.precompute_distances, "tol" : self.tol, "n_jobs" : self.n_jobs,
                                                                        "random_state" : self.random_state}
        else:
            l = {}
        return l

    def predict(self, X):
        s = X.shape
        r = numpy.zeros(s[0] * self.n_clusters)
        r.shape = (s[0], self.n_clusters)
        for k in range(0, s[0]):
            temp = ((X[k] - self.cluster_centers_)**2).sum(1)
            arg = temp.argmin()
            r[k] = temp != temp
            r[k][arg] = 1
        return r

    def score(self, X):
        r = self.predict(X)
        return ((numpy.dot(r, self.cluster_centers_) - X)**2).sum()

    def set_params(self, **params):
        for i in params.keys():
            if i == 'n_clusters':
                self.n_clusters = params[i]
            if i == 'n_init':
                self.n_init = params[i]
            if i == 'n_jobs':
                self.n_jobs = params[i]
            if i == 'tol':
                self.tol = params[i]
            if i == 'precompute_distances':
                self.precompute_distances = params[i]
            if i == 'init':
                self.init = params[i]
            if i == 'max_iter':
                self.max_iter = params[i]
            if i == 'verbose':
                self.verbose = params[i]
            if i == 'random_state':
                self.random_state = params[i]
            if i == 'copy_x':
                self.copy_x = params[i]

        return self


    def transform(self, X, y=None):
        s = X.shape
        Xnew = numpy.zeros(s[0] * self.n_clusters)
        Xnew.shape = (s[0], self.n_clusters)
        for i in range(0, s[0]):
            Xnew[i] = ((X[i] - self.cluster_centers_)**2).sum(1)
        return Xnew

def main():
    artists = get_artists()
    X = get_features(artists)
    bestacc = 0
    bestfscore = 0
    bestacci = 0
    bestfscorei = 0
    l = get_tags_number(artists) + 1
    for e in range(1, l):
        model = Kmeans(n_clusters=e)
        model.fit(X)
        r = model.predict(X)
        y = r.argmax(axis=1)
        acc, fscore = scores(y , artists)
        if acc > bestacc:
            bestacc = acc
            bestacci = e
        if fscore > bestfscore:
            bestfscore = fscore
            bestfscorei = e
    print "best f1score - ", bestfscore,  " for k = ", bestfscorei
    print "best accurancy - ", bestacc,  " for k = ", bestacci




if __name__ == "__main__":
    main()